## Who am I?

I'm Donna, my pronouns are she/her.

Originally from Northern Ireland, I've been living in Scotland for almost 10 years. :mountain:

I'm a Senior Site Reliability Engineer on the Foundations team at GitLab.

## About me

* :mortar_board: I have a BSc in Business Information Technology from Queen's University Belfast.
* :feet: I have two dogs: Skye (Border Collie) and Lewis (Golden Retriever).
* :video_camera: I have an adventure [YouTube channel](https://www.youtube.com/@donnaandelliot).
* :sweat_smile: I started my career in tech by accident. Ask me about it!
* :computer: Whilst I specialise in infrastructure, I've navigated the full tech stack.
* :fire: On my first oncall shift (of my career) a datacentre literally caught on fire.
* :chart_with_upwards_trend: I've automated the deployment of the largest CloudFoundry PaaS in the world (supporting over 1.5k production applications).
* :sparkles: I've ["Sparked API Joy"](https://donna.dev/sparking-api-joy-a-journey-to-confidently-shipping-apis/) by standardising CircleCI's API Infrastructure.

### Hobbies

* Hiking (Munro Bagging: climbing all 282 mountains over 3,000ft in Scotland).
* Videography (sharing adventures on a small YouTube channel).
* Aerial Arts (trapeze, aerial hoop, etc).
* Running.
* Travelling.
* Gardening.
* DIY.

## How I work

I work as asynchronously as possible, but I'm usually near a computer between 9am and 6pm UTC (give or take an hour). My calendar is kept up to date for ease of scheduling.

☕️ Coffee chats are **always welcome.**

### More specifically:

* **Overcommunication**: I work in public channels where possible and will direct work related DMs to the appropriate public channel where possible.
* **Celebrate the smol wins**: No matter how small of an achievement, I will always relish the opportunity to celebrate my team.
* **Whimsy**: I like to inject personality and a bit of whimsy into my work where I can, either through emoji reactions or adding GIFs when reviewing MRs (Thank GIF It's Friday).
* **[A Safe Pair of Hands](https://emauton.org/2022/12/24/a-safe-pair-of-hands/)**: I can be trusted to do the right thing, and ensure everyone is kept updated as I go.
